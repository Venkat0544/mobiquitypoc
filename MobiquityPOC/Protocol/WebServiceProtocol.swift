//
//  WebServiceProtocol.swift
//  MobiquityPOC
//
//  Created by Venkat rao on 23/05/21.
//  Copyright © 2021 Venkat rao. All rights reserved.
//

import Foundation
 protocol WebServiceProtocol
{
    func SuccessResponse(_ json : Codable)
    
    func ErrorResponse(_ error : NSError)
}
